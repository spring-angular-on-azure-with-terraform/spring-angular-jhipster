import { IUser } from './user.model';

export const sampleWithRequiredData: IUser = {
  id: 722,
  login: 'F@y8eBqA\\Aj1LPs',
};

export const sampleWithPartialData: IUser = {
  id: 17161,
  login: '+?@h\\VcH5eG\\oTYkn\\pLkA',
};

export const sampleWithFullData: IUser = {
  id: 18383,
  login: '1_xTP',
};
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
