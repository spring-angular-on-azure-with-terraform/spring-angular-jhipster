import { IAuthority, NewAuthority } from './authority.model';

export const sampleWithRequiredData: IAuthority = {
  name: 'd6db0550-4ebb-46ee-9629-29687220870d',
};

export const sampleWithPartialData: IAuthority = {
  name: '5bad0c2c-2a4f-44b4-bf6b-0884f256d45a',
};

export const sampleWithFullData: IAuthority = {
  name: '15710c0a-075b-4b71-aafa-05ad2f39e642',
};

export const sampleWithNewData: NewAuthority = {
  name: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
